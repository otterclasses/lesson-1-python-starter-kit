import pgzrun
from pgzero.builtins import Actor

WIDTH = 640
HEIGHT = 480

pusheen = Actor('pusheen')
pusheen.pos = 100, 56

def update():
  pusheen.left += 2
  if pusheen.left > WIDTH:
    pusheen.left = 0

def draw():
  # removes everything drawn last loop
  screen.clear()
  # draw our background
  screen.blit('background', (0, 0))
  # draws our kitty 
  pusheen.draw()

pgzrun.go()
