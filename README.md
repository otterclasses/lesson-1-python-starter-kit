# Readme

Readmes typically have information about how to setup a project


# Install dependencies for game

```bash
python -m pip install -r requirements.txt
```

# Run game

```bash
python game/main.py
```